import { userDto } from './../dto/auth.dto';
import * as jwt from "jsonwebtoken"
export const createToken = (data:userDto) => {
    return jwt.sign({data},process.env.SECRET_KEY,{expiresIn:"1h"})
}
export const verifyToken = (token:string) => {
    return jwt.verify(token,process.env.SECRET_KEY);
}