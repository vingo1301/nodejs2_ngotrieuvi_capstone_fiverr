import { ApiProperty } from "@nestjs/swagger"

export interface userDto{
    id: number
    name:          string
    email:         string
    pass_word:     string
    avatar:        string
    phone:         string
    birth_day:     string
    gender:        string
    isAdmin:       boolean
    skill:         string
    certification: string
}
export interface userLoginDto{
    email: string
    pass_word: string
}
export class userBodyDto{
    // @ApiProperty({description:"id",type:Number})
    id: number
    @ApiProperty({description:"Họ Tên",type:String})
    name:          string
    @ApiProperty({description:"Email",type:String})
    email:         string
    @ApiProperty({description:"Mật Khẩu",type:String})
    pass_word:     string
    @ApiProperty({description:"Phone",type:String})
    phone:         string
    avatar:        string
    @ApiProperty({description:"Ngày Sinh",type:String})
    birth_day:     string
    @ApiProperty({description:"Giới Tính",type:String})
    gender:        string
    @ApiProperty({description:"Loại User",type:Boolean})
    isAdmin:       boolean
    @ApiProperty({description:"Kỹ Năng",type:String})
    skill:         string
    @ApiProperty({description:"Chứng Nhận",type:String})
    certification: string
}
export class userLoginBodyDto{
    @ApiProperty({description:"email",type:String})
    email: string
    @ApiProperty({description:"Mật Khẩu",type:String})
    pass_word: string
}