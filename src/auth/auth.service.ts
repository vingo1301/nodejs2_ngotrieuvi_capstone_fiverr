import { createToken } from './jwt/jwt';
import { userDto, userLoginDto } from './dto/auth.dto';
import { Injectable } from '@nestjs/common';
import { PrismaClient } from '@prisma/client'
import * as bcrypt from 'bcrypt'

@Injectable()

export class AuthService {
    
    async createUser(user:userDto){
        const prisma = new PrismaClient()
        let checkEmail = await prisma.nguoiDung.findFirst({
            where:{
                email: user.email
            }
        })
        if(checkEmail !== null){
            return {
                code:400,
                json:{
                    message: "Email đã tồn tại!"
                }
            };
        }else{
            await prisma.nguoiDung.create({data:user})
            user.pass_word = "";
            return {
                code:201,
                json:{
                    message:"Tạo tài khoản thành công!",
                    data:user
                }
            };
        }
    }
    async loginUser(user:userLoginDto){
        const prisma = new PrismaClient();
        let checkEmail = await prisma.nguoiDung.findFirst({
            where:{
                email:user.email
            }
        })
        if(checkEmail == null){
            return {
                code: 404,
                json:{
                    message: "Email không Tồn Tại!"
                }
            }
        }else{
            let checkPass = await bcrypt.compare(user.pass_word,checkEmail.pass_word);
            if(checkPass){
                checkEmail.pass_word = "";
                let token = createToken(checkEmail)
                return {
                    code: 200,
                    json:{
                        message: "Đăng nhập thành công!",
                        data:{
                            user:checkEmail,
                            token
                        }
                    }
                }
            }else{
                return {
                    code: 404,
                    json:{
                        message: "Mật khẩu không chính xác!"
                    }
                }
            }
        }
    }
}
