import { userBodyDto, userLoginBodyDto } from './dto/auth.dto';
import { AuthService } from './auth.service';
import { Controller,Res } from '@nestjs/common';
import {Response} from 'express'
import { ApiTags } from '@nestjs/swagger';
import { Body, Post } from '@nestjs/common/decorators';
import * as bcrypt from 'bcrypt'

@ApiTags("Auth")
@Controller('api/auth')
export class AuthController {
    constructor(
        private authService:AuthService
    ){}
        @Post("signup")
        async createUser(@Body() body:userBodyDto,@Res() res:Response):Promise<any>{
            body.pass_word = await bcrypt.hash(body.pass_word,10);
            let result = this.authService.createUser(body);
            return res.status((await result).code).send((await result).json)
        }
        @Post("signin")
        async loginUser(@Body() body:userLoginBodyDto,@Res() res:Response){
            let result = this.authService.loginUser(body);
            return res.status((await result).code).send((await result).json);
        }
}
