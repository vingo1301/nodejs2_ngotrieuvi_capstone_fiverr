import { async } from 'rxjs';
import { verifyToken } from 'src/auth/jwt/jwt';
import { tokenDto } from 'src/loai-cong-viec/dto/loai-cong-viec.dto';
import { congViecDtoBody } from './dto/cong-viec.dto';
import { Post, Body, Res, Headers } from '@nestjs/common/decorators';
import { Controller, Get, Param, Put, Delete } from '@nestjs/common';
import { CongViecService } from './cong-viec.service';
import { ApiTags } from '@nestjs/swagger';
import { Request, Response } from 'express';

@ApiTags("CongViec")
@Controller('api/cong-viec')
export class CongViecController {
    constructor(
        private congViecService:CongViecService
    ){}
    @Post()
    async createCongViec(@Body() body:congViecDtoBody, @Res() res:Response,@Headers() headers:tokenDto){
        try{
            let {token} = headers;
            let checkToken = verifyToken(token);
            if(checkToken){
                let result = this.congViecService.createCongViec({...body,nguoi_tao:checkToken.data.id});
                return res.status((await result).code).send((await result).content)
            }
        }catch(err){
            return err;
        }
        
    }
    @Get()
    async getCongViec(@Res() res:Response){
        let result = this.congViecService.getCongViec();
        return res.status((await result).code).send((await result).content)
    }
    @Get(":id")
    async getCongViecId(@Res() res:Response,@Param("id") id:number){
        let result = this.congViecService.getCongViecId(id);
        return res.status((await result).code).send((await result).content)
    }
    @Get("lay-theo-chi-tiet-loai/:maChiTietLoai")
    async getCongViecTheoChiTietLoai(@Res() res:Response,@Param("maChiTietLoai") id:number){
        let result = this.congViecService.getCongViecTheoChiTietLoai(id);
        return res.status((await result).code).send((await result).content)
    }
    @Get("lay-theo-ma-loai-cong-viec/:maLoaiCongViec")
    async getCongViecTheoMaLoai(@Res() res:Response,@Param("maLoaiCongViec") id:number){
        let result = this.congViecService.getCongViecTheoMaLoai(id);
        return res.status((await result).code).send((await result).content)
    }
    @Get("lay-theo-ten-cong-viec/:tenCongViec")
    async getCongViecTheoTen(@Res() res:Response,@Param("tenCongViec") key:string){
        let result = this.congViecService.getCongViecTheoTen(key);
        return res.status((await result).code).send((await result).content)
    }
    @Put(":id")
    async updateCongViec(@Param('id') id:number,@Body() body:congViecDtoBody, @Headers() headers:tokenDto,@Res() res:Response){
        let {token} = headers;
        try{
            let checkToken = verifyToken(token);
            if(checkToken){
                let result = this.congViecService.updateCongViec(checkToken.data,body,id);
                res.status((await result).code).send((await result).content)
            }
        }catch(err){
            return res.status(500).send(err);
        }
    }
    @Delete(":id")
    async deleteCongViec(@Param('id') id:number, @Res() res:Response, @Headers() headers:tokenDto){
        let {token} = headers;
        try{
            let checkToken = verifyToken(token);
            if(checkToken){
                let result = this.congViecService.deleteCongViec(id,checkToken.data);
                res.status((await result).code).send((await result).content)
            }
        }catch(err){
            return res.status(500).send(err)
        }
    }
}
