import { ApiProperty } from "@nestjs/swagger"

export interface congViecDto{
    id:                  number
    ten_cong_viec:       string
    danh_gia:            number
    gia_tien:            number
    hinh_anh:            string
    mo_ta:               string
    mo_ta_ngan:          string
    sao_cong_viec:       number
    ma_chi_tiet_loai:    number
    nguoi_tao:           number
}
export class congViecDtoBody{
    id:                  number
    @ApiProperty({type:String})
    ten_cong_viec:       string
    @ApiProperty({type: Number})
    danh_gia:            number
    @ApiProperty({type: Number})
    gia_tien:            number
    @ApiProperty({type:String})
    hinh_anh:            string
    @ApiProperty({type:String})
    mo_ta:               string
    @ApiProperty({type:String})
    mo_ta_ngan:          string
    @ApiProperty({type: Number})
    sao_cong_viec:       number
    @ApiProperty({type: Number})
    ma_chi_tiet_loai:    number
    nguoi_tao:           number
}