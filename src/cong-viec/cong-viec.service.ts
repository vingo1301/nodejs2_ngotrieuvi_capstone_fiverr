import { async } from 'rxjs';
import { PrismaClient } from '@prisma/client';
import { congViecDto } from './dto/cong-viec.dto';
import { Injectable } from '@nestjs/common';
import { userDto } from 'src/auth/dto/auth.dto';

@Injectable()
export class CongViecService {
    prisma = new PrismaClient();
    async createCongViec(congViec:congViecDto){
        try{
            
            let create = await this.prisma.congViec.create({data:congViec});
            return {
                code: 201,
                content: {
                    message: "Tạo Công Việc Thành Công!",
                    data: create
                }
            }
        }catch(err){
            return {
                code: 500,
                content: err
            }
        }
        
    }
    async getCongViec(){
        try{
           let data = await this.prisma.congViec.findMany();
           return {
            code: 200,
            content: data
           }
    }catch(err){
        return {
            code:500,
            content: err
        }
    }
}
async getCongViecId(id:number){
    try{
       let data = await this.prisma.congViec.findFirst({
        where:{
            id:Number(id)
        },
       });
       if(data == null){
        return {
            code: 404,
            content: {
                message:"Không tìm thấy công việc!"
            }
        }
       }
       return {
        code: 200,
        content: data
       }
    }catch(err){
        return {
        code:500,
        content: err
        }
    }
}
async getCongViecTheoChiTietLoai(id:number){
    try{
       let data = await this.prisma.chiTietLoaiCongViec.findMany({
        where:{
            id:Number(id)
        },
        include: {
            congViec: true
        }
       });
       if(data.length == 0){
        return {
            code: 404,
            content: {
                message:"Không tìm thấy mã chi tiết loại!"
            }
        }
       }
       return {
        code: 200,
        content: data
       }
    }catch(err){
        return {
        code:500,
        content: err
        }
    }
}
async getCongViecTheoMaLoai(id:number){
    try{
       let data = await this.prisma.loaiCongViec.findMany({
        where:{
            id:Number(id)
        },
        include: {
            chiTietLoaiCongViec: {
                include: {
                    congViec: true
                }
            }
        }
       });
       if(data.length == 0){
        return {
            code: 404,
            content: {
                message:"Không tìm thấy mã loại công việc!"
            }
        }
       }
       return {
        code: 200,
        content: data
       }
    }catch(err){
        return {
        code:500,
        content: err
        }
    }
}
async getCongViecTheoTen(key:string){
    try{
       let data = await this.prisma.congViec.findMany({
            where:{
                ten_cong_viec:{
                    contains: key
                }
            }
       });
       if(data.length == 0){
        return {
            code: 404,
            content: {
                message:"Không tìm thấy công việc!"
            }
        }
       }
       return {
        code: 200,
        content: data
       }
    }catch(err){
        return {
        code:500,
        content: err
        }
    }
}
async updateCongViec(user:userDto,data:congViecDto,id:number){
    try{
        if(user.isAdmin){
            let update = await this.prisma.congViec.update({
                data,
                where:{
                    id:Number(id)
                }
            })
                return {
                    code: 200,
                    content: {
                        massage: "update thành công!",
                        data:update
                    }
                }
        }else{
            return {
                code: 400,
                content: {
                    message: "User không có quyền admin!"
                }
            }
        }
    }catch(err){
        return {
            code:500,
            content: err
        }
    }
}
async deleteCongViec(id:number,user:userDto){
    try{
        if(user.isAdmin){
            // xoá bình luận
            let binhLuan = await this.prisma.binhLuan.findMany({
                where:{
                    ma_cong_viec: Number(id)
                }
            })
            if(binhLuan.length !==0){
                await this.prisma.binhLuan.deleteMany({
                    where:{
                        ma_cong_viec:Number(id)
                    }
                })
            }
            // xoá thuê công việc
            let thueCongViec = await this.prisma.thueCongViec.findMany({
                where:{
                    ma_cong_viec: Number(id)
                }
            })
            if(thueCongViec.length !== 0){
                await this.prisma.thueCongViec.deleteMany({
                    where:{
                        ma_cong_viec: Number(id)
                    }
                })
            }
            // xoá công việc 
            await this.prisma.congViec.delete({
                where: {
                    id: Number(id)
                }
            })
            return {
                code:200,
                content:{
                    message: "Xoá công việc thành công!"
                }
            }
        }else{
            return {
                code:400,
                content:{
                    messsage: "User không có quyền admin!"
                }
            }
        }
    }catch(err){
        return {
            code:500,
            content: err
        }
    }
}
}
