import { async } from 'rxjs';
import { PrismaClient } from '@prisma/client';
import { Injectable } from '@nestjs/common';
import { userDto } from 'src/auth/dto/auth.dto';

@Injectable()
export class ThueCongViecService {
    prisma = new PrismaClient();
    async createThueCongViec(user:userDto,data:any){
        try{
            data.ma_nguoi_thue = user.id;
            data.hoan_thanh = false;
            let create = await this.prisma.thueCongViec.create({data});
            return {
                code:201,
                content:{
                    message: "Thuê công việc thành công!",
                    data:create
                }
            }
        }catch(err){
            return {
                code:500,
                content:err
            }
        }
    }
    async getThueCongViec(){
        try{
            let data = await this.prisma.thueCongViec.findMany();
            return {
                code:200,
                content: data
            }
        }catch(err){
            return {
                code:500,
                content:err
            }
        }
    }
    async getThueCongViecId(id:number){
        try{
            let data = await this.prisma.thueCongViec.findFirst({
                where:{
                    id:Number(id)
                }
            });
            if(data){
                return {
                    code:200,
                    content: data
                }
            }else{
                return{
                    code:404,
                    content:{
                        message: "Không tìm thấy id"
                    }
                }
            }
            
        }catch(err){
            return {
                code:500,
                content:err
            }
        }
    }
    async hoanThanhCongViec(id:number){
        let data = {
            hoan_thanh:true
        }
        try{
            let update = await this.prisma.thueCongViec.update({
                data,
                where:{
                    id: Number(id)
                }
            })
            if(update){
                return {
                    code:200,
                    content: {
                        message: "Update thành công!",
                        data:update
                    }
                }
            }
        }catch(err){
            return {
                code:500,
                content:err
            }
        }
    }
    async updateThueCongViec(id:number,data:any,user:userDto){
        try{
            if(user.isAdmin){
                let update = await this.prisma.thueCongViec.update({
                    data,
                    where:{
                        id:Number(id)
                    }
                })
                if(update){
                    return {
                        code:200,
                        content: {
                            message: "update thành công!",
                            data: update
                        }
                    }
                }
            }else{
                return {
                    code: 400,
                    content: {
                        message: "User Không có quyền admin!"
                    }
                }
            }
            
        }catch(err){
            return {
                code:500,
                content: err
            }
        }
    }
    async deleteThueCongViec(id:number,user:userDto){
        try{
            if(user.isAdmin){
                let xoa = await this.prisma.thueCongViec.delete({
                    where:{
                        id: Number(id)
                    }
                })
                return {
                    code:200,
                    content:{
                        massage: "Xoá thành công!",
                        data:xoa
                    }
                }
            }else{
                return {
                    code:400,
                    content: {
                        message: "user không có quyền admin!"
                    }
                }
            }
        }catch(err){
            return {
                code:500,
                content:err
            }
        }
    }
}
