import { ApiProperty } from "@nestjs/swagger"

export interface thueCongViecDto{
  id:            number
  ma_cong_viec:  number
  ma_nguoi_thue: number
  ngay_thue:     Date
  hoan_thanh:    Boolean
}
export class thueCongViecDtoBody{
    id:           number
    @ApiProperty({type:Number})
    ma_cong_viec:  number
    @ApiProperty({type: Number})
    ma_nguoi_thue: number
    @ApiProperty({type:Date})
    ngay_thue:     Date
    @ApiProperty({type:Boolean})
    hoan_thanh:    Boolean
  }