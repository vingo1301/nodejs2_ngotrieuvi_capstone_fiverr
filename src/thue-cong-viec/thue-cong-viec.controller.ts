import { async } from 'rxjs';
import { verifyToken } from './../auth/jwt/jwt';
import { thueCongViecDtoBody } from './dto/thue-cong-viec.dto';
import { ThueCongViecService } from './thue-cong-viec.service';
import { Body, Controller, Headers, Post, Res, Param, Put, Delete } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { tokenDto } from 'src/loai-cong-viec/dto/loai-cong-viec.dto';
import { Response } from 'express';
import { Get } from '@nestjs/common/decorators';

@ApiTags("ThueCongViec")
@Controller('api/thue-cong-viec')
export class ThueCongViecController {
    constructor(
        private thueCongViecService:ThueCongViecService
    ){}
    @Post()
    async createThueCongViec(@Headers() headers:tokenDto,@Body() body:thueCongViecDtoBody, @Res() res:Response){
        let {token} = headers;
        try{
            let checkToken = verifyToken(token);
            if(checkToken){
                let result = this.thueCongViecService.createThueCongViec(checkToken.data,body)
                return res.status((await result).code).send((await result).content)
            }
        }catch(err){
            return res.status(500).send(err);
        }
    }
    @Get()
    async getThueCongViec(@Res() res:Response){
        let result = this.thueCongViecService.getThueCongViec();
        return res.status((await result).code).send((await result).content)
    }
    @Get(":id")
    async getThueCongViecId(@Param('id') id:number,@Res() res:Response){
        let result = this.thueCongViecService.getThueCongViecId(id);
        return res.status((await result).code).send((await result).content);
    }
    @Post("hoan-thanh-cong-viec/:id")
    async hoanThanhCongViec(@Param('id') id:number,@Res() res:Response){
        let result = this.thueCongViecService.hoanThanhCongViec(id);
        return res.status((await result).code).send((await result).content)
    }
    @Put(':id')
    async updateThueCongViec(@Param('id') id:number, @Body() body:thueCongViecDtoBody,@Headers() headers:tokenDto, @Res() res:Response){
        let {token} = headers;
        try{
            let checkToken = await verifyToken(token);
        if(checkToken){
            let result = this.thueCongViecService.updateThueCongViec(id,body,checkToken.data);
            return res.status((await result).code).send((await result).content);
        }
        }catch(err){
            return res.status(500).send(err);
        }
        
    }
    @Delete(":id")
    async deleteThueCongViec(@Param('id') id:number,@Headers() headers:tokenDto, @Res() res:Response){
        let {token} = headers;
        try{
            let checkToken = await verifyToken(token);
            if(checkToken){
                let result = this.thueCongViecService.deleteThueCongViec(id,checkToken.data);
                return res.status((await result).code).send((await result).content);
            }
        }catch(err){
            return res.status(500).send(err);
        }
    }
}
