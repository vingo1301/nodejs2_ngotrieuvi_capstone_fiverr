import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import { ConfigModule } from '@nestjs/config';
import { LoaiCongViecModule } from './loai-cong-viec/loai-cong-viec.module';
import { ChiTietLoaiCongViecModule } from './chi-tiet-loai-cong-viec/chi-tiet-loai-cong-viec.module';
import { CongViecModule } from './cong-viec/cong-viec.module';
import { ThueCongViecModule } from './thue-cong-viec/thue-cong-viec.module';
import { BinhLuanModule } from './binh-luan/binh-luan.module';

@Module({
  imports: [UserModule, AuthModule,ConfigModule.forRoot({isGlobal:true}), LoaiCongViecModule, ChiTietLoaiCongViecModule, CongViecModule, ThueCongViecModule, BinhLuanModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
