import { async } from 'rxjs';
import { PrismaClient } from '@prisma/client';
import { Injectable } from '@nestjs/common';
import { loaiCVDto } from './dto/loai-cong-viec.dto';
import { userDto } from 'src/auth/dto/auth.dto';
import { rejects } from 'assert';

@Injectable()
export class LoaiCongViecService {
    async createLoaiCongViec(jobs:loaiCVDto){
        const prisma = new PrismaClient();
        let loaiCongViec = await prisma.loaiCongViec.create({data:jobs});
        return {
            code:201,
            json:{
                message:"Tạo Loại Công Việc Thành Công!",
                data:loaiCongViec
            }
        }
    }
    async getLoaiCongViec(){
        const prisma = new PrismaClient();
        let data = await prisma.loaiCongViec.findMany();
        return {
            code:200,
            content: data
        }
    }
    async getLoaiCongViecId(id:number){
        const prisma = new PrismaClient();
        let data = await prisma.loaiCongViec.findFirst({
            where: {
                id: Number(id)
            }
        });
        if(data == null){
            return {
                code:200,
                content: "Không tìm thấy loại công việc!"
            }
        }else{
            return {
                code:200,
                content: data
            }
        }
    }
    async updateLoaiCongViec(id:number,userData:userDto,loaiCongViecData:loaiCVDto){
        const prisma = new PrismaClient();
        let checkUser = userData.isAdmin;
        if(checkUser){
            let update = await prisma.loaiCongViec.update({
                data:loaiCongViecData,
                where: {
                    id: Number(id)
                }
            })
            return {
                code: 200,
                content: {
                    message: "Update thành công!",
                    data:update
                }
            }
        }else{
            return {
                code:400,
                content:{
                    message: "User không có quyền admin!"
                }
                
            }
        }
    }
    async deleteLoaiCongViec(id:number,userData:userDto){
        const prisma = new PrismaClient();
        let checkUser = userData.isAdmin;
        if(checkUser){
            let checkChiTiet = await prisma.chiTietLoaiCongViec.findMany({
                where:{
                    ma_loai_cong_viec: Number(id)
                }
            })
            
            if(checkChiTiet.length == 0){
                let delLCV = await prisma.loaiCongViec.delete({
                    where:{
                        id:Number(id)
                    }
                })
                return {
                    code: 200,
                    content:{
                        message:"Xoá Loại Công Việc Thành Công!",
                        data: delLCV
                    }
                }
            }else{
                return {
                    code:400,
                    content:{
                        message:"Thao tác thất bại! Vui lòng xoá hết ChiTietLoaiCongViec trước.",
                        data: checkChiTiet
                    }
                }
            }
        }else{
            return {
                code:400,
                content:{
                    message: "User không có quyền admin!"
                }
                
            }
        }
    }
    async getMenu(){
        const prisma = new PrismaClient();
        let data = await prisma.loaiCongViec.findMany({
            include:{
                chiTietLoaiCongViec: true
            }
        })
        return {
            code:200,
            content:{
                data
            }
        }
    }
}
