import { async } from 'rxjs';
import { verifyToken } from './../auth/jwt/jwt';
import { loaiCVBodyDto, tokenDto } from './dto/loai-cong-viec.dto';
import { Body, Get, Param, Post, Res, Put, Headers, Delete } from '@nestjs/common/decorators';
import { LoaiCongViecService } from './loai-cong-viec.service';
import { Controller, HttpStatus } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Response } from 'express';

@ApiTags("LoaiCongViec")
@Controller('api/loai-cong-viec')
export class LoaiCongViecController {
    constructor(
        private loaiCongViec:LoaiCongViecService
    ){}
    @Get()
    async getLoaiCongViec(@Res() res:Response){
        let result = this.loaiCongViec.getLoaiCongViec();
        return res.status((await result).code).send((await result).content)
    }
    @Get("lay-loai-cong-viec/:id")
    async getLoaiCongViecId(@Res() res:Response,@Param('id') id:number){
        let result = this.loaiCongViec.getLoaiCongViecId(id);
        return res.status((await result).code).send((await result).content)
    }

    @Post()
    async createLoaiCongViec(@Body() body:loaiCVBodyDto,@Res() res:Response){
        let result = this.loaiCongViec.createLoaiCongViec(body);
        return res.status((await result).code).send((await result).json)
    }
    @Put(":id")
    async updateLoaiCongViec(@Headers() headers:tokenDto,@Body() body:loaiCVBodyDto,@Param('id') id:number,@Res() res:Response){
        try{
            let {token} = headers;
            let checkToken = await verifyToken(token);
            if(checkToken){
                let result = this.loaiCongViec.updateLoaiCongViec(id,checkToken.data,body)
                return res.status((await result).code).send((await result).content)
            }
        }catch(err){
            return res.status(400).send(err);
        }
    }
    @Delete(":id")
    async deleteLoaiCongViec(@Headers() headers:tokenDto,@Param('id') id:number,@Res() res:Response){
        try{
            let {token} = headers;
            let checkToken = await verifyToken(token);
            if(checkToken){
                let result = this.loaiCongViec.deleteLoaiCongViec(id,checkToken.data);
                res.status((await result).code).send((await result).content)
            }
        }catch(err){
            return res.status(400).send(err);
        }
    }
    @Get("lay-menu-loai-cong-viec")
    async getMenu(@Res() res:Response){
        let result = this.loaiCongViec.getMenu();
        return res.status((await result).code).send((await result).content)
    }
}
