import { ApiProperty } from "@nestjs/swagger"

export interface loaiCVDto{
    id: number
    ten_loai_cong_viec: string
}
export class loaiCVBodyDto{
    id: number
    @ApiProperty({type:String})
    ten_loai_cong_viec: string
}
export class tokenDto{
    @ApiProperty({type:String})
    token: string
}