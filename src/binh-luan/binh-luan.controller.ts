import { verifyToken } from './../auth/jwt/jwt';
import { binhLuanBodyDto } from './dto/binh-luan.dto';
import { tokenDto } from './../loai-cong-viec/dto/loai-cong-viec.dto';
import { async } from 'rxjs';
import { BinhLuanService } from './binh-luan.service';
import { Body, Controller, Get, Headers, Param, Post, Put, Res, Delete } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Response } from 'express';

@ApiTags("binhLuan")
@Controller('api/binh-luan')
export class BinhLuanController {
    constructor(
        private binhLuanService:BinhLuanService
    ){}
    @Post()
    async createBinhLuan(@Headers() headers:tokenDto,@Body() body:binhLuanBodyDto,@Res() res: Response){
        let {token} = headers;
        try{
            let checkToken = await verifyToken(token);
            if(checkToken){
                let result = this.binhLuanService.createBinhLuan(checkToken.data,body);
                return res.status((await result).code).send((await result).content)
            }
        }catch(err){
            return res.status(500).send(err);
        }
    }
    @Get()
    async getBinhLuan(@Res() res:Response){
        let result = this.binhLuanService.getBinhLuan();
        return res.status((await result).code).send((await result).content)
    }
    @Get("lay-theo-ma-cong-viec/:maCongViec")
    async getBinhLuanTheoCongViec(@Param('maCongViec') id:number,@Res() res:Response){
        let result = this.binhLuanService.getBinhLuanTheoCongViec(id);
        return res.status((await result).code).send((await result).content)
    }
    @Put(":id")
    async updateBinhLuan(@Param('id') id:number,@Headers() headers:tokenDto,@Body() body:binhLuanBodyDto,@Res() res:Response){
        let {token} = headers;
        try{
            let checkToken = await verifyToken(token);
            if(checkToken){
                let result = this.binhLuanService.updateBinhLuan(id,checkToken.data,body);
                return res.status((await result).code).send((await result).content)
            }
        }catch(err){
            return res.status(500).send(err);
        }
    }
    @Delete(":id")
    async deleteBinhLuan(@Param('id') id:number, @Headers() headers:tokenDto, @Res() res:Response){
        let {token} = headers;
        try{
            let checkToken = await verifyToken(token);
            if(checkToken){
                let result = this.binhLuanService.deleteBinhLuan(id,checkToken.data)
                return res.status((await result).code).send((await result).content);
            }
        }catch(err){
            return res.status(500).send(err)
        }
    }
}
