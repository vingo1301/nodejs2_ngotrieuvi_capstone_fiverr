import { async } from 'rxjs';
import { binhLuanDto } from './dto/binh-luan.dto';
import { userDto } from './../auth/dto/auth.dto';
import { PrismaClient } from '@prisma/client';
import { Injectable } from '@nestjs/common';

@Injectable()
export class BinhLuanService {
    prisma = new PrismaClient();
    async createBinhLuan(user:userDto,data:binhLuanDto){
        try{
            data.ma_nguoi_binh_luan = user.id;
            let create = await this.prisma.binhLuan.create({
            data
        })
        return {
            code:201,
            content: {
                message: "Đăng bình luận thành công!",
                data: create
            }
        }
        }catch(err){
            return {
                code: 500,
                content: err
            }
        }
    }
    async getBinhLuan(){
        try{
            let data = await this.prisma.binhLuan.findMany({
                include:{
                    nguoiDung:{
                        select:{
                            name:true,
                            avatar:true
                        }
                    }
                }
            });
            return {
                code:200,
                content:{
                    data
                }
            }
        }catch(err){
            return {
                code:500,
                content: err
            }

        }
    }
    async getBinhLuanTheoCongViec(id:number){
        try{
            let data = await this.prisma.binhLuan.findMany({
                where:{
                    ma_cong_viec: Number(id)
                },
                include:{
                    nguoiDung:{
                        select:{
                            name:true,
                            avatar:true
                        }
                    }
                }
            });
            return {
                code:200,
                content:{
                    data
                }
            }
        }catch(err){
            return {
                code:500,
                content: err
            }

        }
    }
    async updateBinhLuan(id:number,user:userDto,data:binhLuanDto){
        try{
            let findUser = await this.prisma.binhLuan.findFirst({
                where:{
                    id:Number(id),
                    ma_nguoi_binh_luan: user.id
                }
            }) 
            if(user.isAdmin || findUser){
                let update = await this.prisma.binhLuan.update({
                    data,
                    where:{
                        id:Number(id)
                    }
                })
                return {
                    code:200,
                    content: {
                        message: "update thành công!",
                        data:update
                    }
                }
            }else{
                return {
                    code:400,
                    content: "Chỉ có người bình luận hoặc admin mới được phép sửa bình luận."
                }
            }
        }catch(err){
            return {
                code: 500,
                content: err
            }
        }
    }
    async deleteBinhLuan(id:number,user:userDto){
        try{
            let findUser = await this.prisma.binhLuan.findFirst({
                where:{
                    id:Number(id),
                    ma_nguoi_binh_luan: user.id
                }
            }) 
            if(user.isAdmin || findUser){
                let delBinhLuan = await this.prisma.binhLuan.delete({
                    where:{
                        id:Number(id)
                    }
                })
                return {
                    code:200,
                    content: {
                        message: "xoá thành công!",
                        data:delBinhLuan
                    }
                }
            }else{
                return {
                    code:400,
                    content: "Chỉ có người bình luận hoặc admin mới được phép xoá bình luận."
                }
            }
        }catch(err){
            return {
                code: 500,
                content: err
            }
        }
    }
}
