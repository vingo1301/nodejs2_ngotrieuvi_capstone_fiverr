import { ApiProperty } from "@nestjs/swagger"

export interface binhLuanDto{
    id:                 number
    ma_cong_viec:       number
    ma_nguoi_binh_luan: number
    ngay_binh_luan:     Date
    noi_dung:           string
    sao_binh_luan:      number
}
export class binhLuanBodyDto{
    id:                 number
    @ApiProperty({type:Number})
    ma_cong_viec:       number
    ma_nguoi_binh_luan: number
    @ApiProperty({type:Date})
    ngay_binh_luan:     Date
    @ApiProperty({type:String})
    noi_dung:           string
    @ApiProperty({type:Number})
    sao_binh_luan:      number
}