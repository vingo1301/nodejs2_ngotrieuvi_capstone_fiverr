import { async } from 'rxjs';
import { UserService } from './user.service';
import { Controller, Get, Param, Res } from '@nestjs/common';
import { ApiParam, ApiTags } from '@nestjs/swagger';
import {Response} from 'express'

@ApiTags("NguoiDung")
@Controller('api/user')
export class UserController {
    constructor(
        private userService:UserService
    ){}
        @Get()
        async getUser(@Res() res:Response):Promise<any> {
            return res.status(200).send(await this.userService.getUser())
        }
        @Get(":id")
        async getUserId(@Param('id') id:number,@Res() res:Response):Promise<any>{
            let result = await this.userService.getUserId(id);
            res.status(result.code).send(result.data)
            
        }
}
