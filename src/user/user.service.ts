import { async } from 'rxjs';

import { PrismaClient } from '@prisma/client';
import { Injectable } from '@nestjs/common';
import { userDto } from 'src/auth/dto/auth.dto';

@Injectable()
export class UserService {
    async getUser():Promise<userDto[]>{
        const prisma = new PrismaClient();
        let user = await prisma.nguoiDung.findMany();
        user.map((item) => {
            item.pass_word = "";
        })
        return user;
    }
    async getUserId(id:number){
        const prisma = new PrismaClient();
        let user = await prisma.nguoiDung.findFirst({
            where:{
                id: Number(id)
            }
        })
        if(user){
            user.pass_word = "";
            return {
                code:200,
                data: user
            }
        }else{
            return{
                code:404,
                data:"Không tìm thấy user!"
            }
        }
    }
}
