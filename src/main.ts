import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import * as express from "express"

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  app.use(express.static("."))
  // setup Swagger
  const config = new DocumentBuilder()
    .setTitle('Swagger Fiverr')
    .setVersion('1.0')
    .addTag('Fiverr')
    .build();
    const document = SwaggerModule.createDocument(app, config);
    SwaggerModule.setup('api', app, document);
  // end Swagger
  await app.listen(8080);
}
bootstrap();
