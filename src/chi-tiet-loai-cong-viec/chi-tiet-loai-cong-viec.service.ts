import { PrismaClient } from '@prisma/client';
import { chiTietLCV } from './dto/chiTietLCV.dto';
import { async } from 'rxjs';
import { Injectable } from '@nestjs/common';
import { userDto } from 'src/auth/dto/auth.dto';

@Injectable()
export class ChiTietLoaiCongViecService {
    async createChiTiet(chiTietData:chiTietLCV){
        const prisma = new PrismaClient();
        //  kiểm tra mã loại CV có tồn tại không
        let checkLoaiCongViec = await prisma.loaiCongViec.findFirst({
            where:{
                id: chiTietData.ma_loai_cong_viec
            }
        })
        if(checkLoaiCongViec){
            let create = await prisma.chiTietLoaiCongViec.create({data:chiTietData});
            return {
                code: 201,
                content:{
                    message: "Tạo chiTietLoaiCongViec thành công!",
                    data:create
                }
            }
        }else{
            return {
                code:404,
                content:{
                    message:"Không tìm thấy ma_loai_cong_viec."
                }
            }
        }
    }
    async getChiTiet(){
        const prisma = new PrismaClient();
        let data = await prisma.chiTietLoaiCongViec.findMany();
        return {
            code:200,
            content:{
                data
            }
        }
    }
    async getChiTietId(id:number){
        const prisma = new PrismaClient();
        let data = await prisma.chiTietLoaiCongViec.findFirst({
            where:{
                id:Number(id)
            }
        });
        if(data){
            return {
                code:200,
                content:{
                    data
                }
            }
        }else{
            return{
                code:404,
                content:{
                    message: "Không tìm thấy ChiTietLoaiCongViec!"
                }
            }
        }
        
    }
    async getChiTietMaLCV(id:number){
        const prisma = new PrismaClient();
        let data = await prisma.chiTietLoaiCongViec.findMany({
            where:{
                ma_loai_cong_viec:Number(id)
            }
        });
        if(data.length !==0){
            return {
                code:200,
                content:{
                    data
                }
            }
        }else{
            return{
                code:404,
                content:{
                    message: "Không tìm thấy ChiTietLoaiCongViec!"
                }
            }
        }
        
    }
    async updateChiTietLCV(id:number,chiTietData:chiTietLCV,userData:userDto){
        const prisma = new PrismaClient();
        if(userData.isAdmin){
            //  kiểm tra mã loại CV có tồn tại không
            let checkLoaiCongViec = await prisma.loaiCongViec.findFirst({
                where:{
                id: chiTietData.ma_loai_cong_viec
             }
            })
            if(checkLoaiCongViec){
                let update = await prisma.chiTietLoaiCongViec.update({
                    data:chiTietData,
                    where:{
                        id: Number(id)
                    }
                });
                return {
                    code: 201,
                    content:{
                        message: "Cập nhật chiTietLoaiCongViec thành công!",
                        data:update
                    }
                }
            }else{
                return {
                    code:404,
                    content:{
                        message:"Không tìm thấy ma_loai_cong_viec."
                    }
                }
            }
        }else{
            return {
                code: 401,
                content:{
                    message:"User không có quyền admin!"
                }
            }
        }
    }
    async deleteChiTietLCV (id:number,user:userDto){
        const prisma = new PrismaClient();
        if(user.isAdmin){
            // kiểm tra id có chính xác không
            let checkId = await prisma.chiTietLoaiCongViec.findFirst({
                where: {
                    id:Number(id)
                }
            })
            if(checkId){
                //  kiểm tra có tạo công việc chưa
                let checkCongViec = await prisma.congViec.findMany({
                    where:{
                        ma_chi_tiet_loai: Number(id)
                    }
                })
                if(checkCongViec.length == 0){
                    let deleteResult = await prisma.chiTietLoaiCongViec.delete({
                        where:{
                            id:Number(id)
                        }
                    })
                    return {
                        code: 200,
                        content: {
                            message:"Xoá thành công!",
                            data: deleteResult
                        }
                    }
                }else{
                    return {
                        code:400,
                        content:{
                            message:"Thao tác thất bại! Vui lòng xoá tất cả CongViec trong chiTietLoaiCongViec.",
                            data:checkCongViec
                        }
                    }
                }
                
            }else{
                return {
                    code: 404,
                    content:{
                        message:"Không tìm thấy chiTietLoaiCongViec!"
                    }
                }
            }
        }else{
            return {
                code:400,
                content:{
                    message:"User không có quyền Admin!"
                }
            }
        }
    }
}
