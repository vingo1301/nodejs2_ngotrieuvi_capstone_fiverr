import { async } from 'rxjs';
import { ChiTietLoaiCongViecService } from './chi-tiet-loai-cong-viec.service';
import { Body, Controller, Get, Post, Res, Param, Put, Headers, Delete } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Response } from 'express';
import { chiTietLCVBody } from './dto/chiTietLCV.dto';
import { tokenDto } from 'src/loai-cong-viec/dto/loai-cong-viec.dto';
import { verifyToken } from 'src/auth/jwt/jwt';

@ApiTags("ChiTietLoaiCongViec")
@Controller('api/chi-tiet-loai-cong-viec')
export class ChiTietLoaiCongViecController {
    constructor(
        private chiTietLoaiCongViecService:ChiTietLoaiCongViecService
    ){}
    @Post()
    async createChiTietLCV(@Body() body:chiTietLCVBody, @Res() res:Response){
        try{
            let result = this.chiTietLoaiCongViecService.createChiTiet(body);
        return res.status((await result).code).send((await result).content);
        }catch(err){
            return res.status(500).send(err)
        }
    }
    @Get()
    async getChiTietLCV(@Res() res:Response){
        try{
            let result = this.chiTietLoaiCongViecService.getChiTiet();
            return res.status((await result).code).send((await result).content);
        }catch(err){
            return res.status(500).send(err)
        }
    }
    @Get(":id")
    async getChiTietLCVId(@Res() res:Response,@Param('id') id:number){
        try{
            let result = this.chiTietLoaiCongViecService.getChiTietId(id);
            return res.status((await result).code).send((await result).content);
        }catch(err){
            return res.status(500).send(err)
        }
    }
    @Get("lay-theo-ma-loai-cong-viec/:maLoaiCongViec")
    async getChiTietMaLCV(@Res() res:Response,@Param('maLoaiCongViec') id:number){
        try{
            let result = this.chiTietLoaiCongViecService.getChiTietMaLCV(id);
            return res.status((await result).code).send((await result).content);
        }catch(err){
            return res.status(500).send(err)
        }
    }
    @Put(":id")
    async updateChiTietLCV(@Res() res:Response,@Param('id') id:number,@Body() body:chiTietLCVBody,@Headers() headers:tokenDto){
        try{
            let {token} = headers;
            let checkToken = verifyToken(token);
            if(checkToken){
                let result = this.chiTietLoaiCongViecService.updateChiTietLCV(id,body,checkToken.data);
                return res.status((await result).code).send((await result).content)
            }
        }catch(err){
            return res.status(500).send(err)
        }
    }
    @Delete(":id")
    async deleteChiTietLCV(@Res() res:Response,@Param('id') id:number,@Headers() headers:tokenDto){
        try{
            let {token} = headers;
            let checkToken = verifyToken(token);
            if(checkToken){
               let result = this.chiTietLoaiCongViecService.deleteChiTietLCV(id,checkToken.data);
               return res.status((await result).code).send((await result).content)
            }
        }catch(err){
            return res.status(500).send(err)
        }
    }
}
