import { ApiProperty } from '@nestjs/swagger';
export interface chiTietLCV{
    id:                number
    ten_chi_tiet:      string
    hinh_anh:          string
    ma_loai_cong_viec: number
}
export class chiTietLCVBody{
    id:                number
    @ApiProperty({type:String})
    ten_chi_tiet:      string
    @ApiProperty({type:String})
    hinh_anh:          string
    @ApiProperty({type:Number})
    ma_loai_cong_viec: number
}