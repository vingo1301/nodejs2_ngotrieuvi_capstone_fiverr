-- MySQL dump 10.13  Distrib 8.0.31, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: db_fiverr
-- ------------------------------------------------------
-- Server version	8.0.31

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `BinhLuan`
--

DROP TABLE IF EXISTS `BinhLuan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `BinhLuan` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ma_cong_viec` int NOT NULL,
  `ma_nguoi_binh_luan` int NOT NULL,
  `ngay_binh_luan` datetime(3) NOT NULL,
  `noi_dung` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sao_binh_luan` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `BinhLuan_ma_nguoi_binh_luan_fkey` (`ma_nguoi_binh_luan`),
  KEY `BinhLuan_ma_cong_viec_fkey` (`ma_cong_viec`),
  CONSTRAINT `BinhLuan_ma_cong_viec_fkey` FOREIGN KEY (`ma_cong_viec`) REFERENCES `CongViec` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `BinhLuan_ma_nguoi_binh_luan_fkey` FOREIGN KEY (`ma_nguoi_binh_luan`) REFERENCES `NguoiDung` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `BinhLuan`
--

LOCK TABLES `BinhLuan` WRITE;
/*!40000 ALTER TABLE `BinhLuan` DISABLE KEYS */;
INSERT INTO `BinhLuan` VALUES (3,2,2,'2023-03-14 12:53:37.521','Xuất Sắc. Làm việc rất có tâm',5),(4,3,2,'2023-03-14 13:01:40.656','Hoàn thành công việc đúng hạn. Nhiệt tình.',5);
/*!40000 ALTER TABLE `BinhLuan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ChiTietLoaiCongViec`
--

DROP TABLE IF EXISTS `ChiTietLoaiCongViec`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ChiTietLoaiCongViec` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ten_chi_tiet` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hinh_anh` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ma_loai_cong_viec` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ChiTietLoaiCongViec_ma_loai_cong_viec_fkey` (`ma_loai_cong_viec`),
  CONSTRAINT `ChiTietLoaiCongViec_ma_loai_cong_viec_fkey` FOREIGN KEY (`ma_loai_cong_viec`) REFERENCES `LoaiCongViec` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ChiTietLoaiCongViec`
--

LOCK TABLES `ChiTietLoaiCongViec` WRITE;
/*!40000 ALTER TABLE `ChiTietLoaiCongViec` DISABLE KEYS */;
INSERT INTO `ChiTietLoaiCongViec` VALUES (1,'Social Media Marketing','https://fiverrnew.cybersoft.edu.vn/images/lcv3.jpg',1),(2,'Influencer Marketing','https://fiverrnew.cybersoft.edu.vn/images/lcv3.jpg',1),(3,'Social Media Advertising','https://fiverrnew.cybersoft.edu.vn/images/lcv4.jpg',2),(4,'Search Engine Marketing (SEM)','https://fiverrnew.cybersoft.edu.vn/images/lcv4.jpg',2),(5,'Articles & Blog Posts','http://fiverrnew.cybersoft.edu.vn/images/30-10-2022-07-39-22-application-development_2x.jpg',3),(6,'Video Editing','https://fiverrnew.cybersoft.edu.vn/images/lcv8.jpg',4),(7,'Video Editing','https://fiverrnew.cybersoft.edu.vn/images/lcv7.jpg',3),(8,'Short Video Ads','https://fiverrnew.cybersoft.edu.vn/images/lcv7.jpg',4),(9,'Brand Voice & Tone','https://fiverrnew.cybersoft.edu.vn/images/lcv6.jpg',5),(10,'Songwriters','https://fiverrnew.cybersoft.edu.vn/images/lcv9.jpg',5);
/*!40000 ALTER TABLE `ChiTietLoaiCongViec` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CongViec`
--

DROP TABLE IF EXISTS `CongViec`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CongViec` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ten_cong_viec` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `danh_gia` int DEFAULT NULL,
  `gia_tien` int NOT NULL,
  `hinh_anh` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mo_ta` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mo_ta_ngan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sao_cong_viec` int DEFAULT NULL,
  `ma_chi_tiet_loai` int NOT NULL,
  `nguoi_tao` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `CongViec_nguoi_tao_fkey` (`nguoi_tao`),
  KEY `CongViec_ma_chi_tiet_loai_fkey` (`ma_chi_tiet_loai`),
  CONSTRAINT `CongViec_ma_chi_tiet_loai_fkey` FOREIGN KEY (`ma_chi_tiet_loai`) REFERENCES `ChiTietLoaiCongViec` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `CongViec_nguoi_tao_fkey` FOREIGN KEY (`nguoi_tao`) REFERENCES `NguoiDung` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CongViec`
--

LOCK TABLES `CongViec` WRITE;
/*!40000 ALTER TABLE `CongViec` DISABLE KEYS */;
INSERT INTO `CongViec` VALUES (2,'I will design an outstanding logo',100,15,'https://fiverrnew.cybersoft.edu.vn/images/cv1.jpg','\nHi There,\r\n\r\n\r\n\r\nHave You Been Looking for a Brand Logo with Complete Corporate Brand Identity?\r\n\r\n\r\n\r\nHi, I am Talha, a Passionate Professional Graphic Designer designing brand logos & brand Identity for many Years, I have successfully designed brand logos with the complete corporate brand identity for different brands around the world.\r\n\r\n\r\n\r\nI also would love to do demanded designs, Just leave your request, I will respond instantly.','Plus - MOST SELLING!\r\nUS$65\r\n3 logo options + source file in Ai, EPS, SVG, PDF, and PSD\r\n\r\n2 Days Delivery\r\n5 Revisions\r\n3 concepts included\r\nLogo transparency\r\nVector file\r\nPrintable file\r\nSource file',1,1,2),(3,'I will design 3 modern minimalist flat logo designs',19907,30,'https://fiverrnew.cybersoft.edu.vn/images/cv2.jpg','\nHi There,\r\n\r\n\r\n\r\nHave You Been Looking for a Brand Logo with Complete Corporate Brand Identity?\r\n\r\n\r\n\r\nHi, I am Talha, a Passionate Professional Graphic Designer designing brand logos & brand Identity for many Years, I have successfully designed brand logos with the complete corporate brand identity for different brands around the world.\r\n\r\n\r\n\r\nI also would love to do demanded designs, Just leave your request, I will respond instantly.','US$30\r\nSave up to 15% with Subscribe to Save\r\nSTANDARD Package - Recommended 5 ULTRA HQ logos + Free Revisions + Vector Source files(Ai, EPS, PDF) for final design\r\n\r\n2 Days Delivery\r\nUnlimited Revisions',2,1,2),(5,'I will design minimal logo with complete corporate brand identity',66,10,'https://fiverrnew.cybersoft.edu.vn/images/cv3.jpg','\nHi There,\r\n\r\n\r\n\r\nHave You Been Looking for a Brand Logo with Complete Corporate Brand Identity?\r\n\r\n\r\n\r\nHi, I am Talha, a Passionate Professional Graphic Designer designing brand logos & brand Identity for many Years, I have successfully designed brand logos with the complete corporate brand identity for different brands around the world.\r\n\r\n\r\n\r\nI also would love to do demanded designs, Just leave your request, I will respond instantly.','US$10\r\nSave up to 20% with Subscribe to Save\r\nBASIC CORPORATE BRAND IDENTITY Business Card + Letterhead + Compliment Design\r\n\r\n2 Days Delivery\r\nUnlimited Revisions\r\nIncludes logo design\r\nLogo usage guidelines\r\nColor palette\r\nTypography guidelines',4,2,1),(6,'I will do modern logo design with premium brand identity',422,35,'https://fiverrnew.cybersoft.edu.vn/images/cv4.jpg','\nHi There,\r\n\r\n\r\n\r\nHave You Been Looking for a Brand Logo with Complete Corporate Brand Identity?\r\n\r\n\r\n\r\nHi, I am Talha, a Passionate Professional Graphic Designer designing brand logos & brand Identity for many Years, I have successfully designed brand logos with the complete corporate brand identity for different brands around the world.\r\n\r\n\r\n\r\nI also would love to do demanded designs, Just leave your request, I will respond instantly.','US$10\r\nSave up to 20% with Subscribe to Save\r\nBASIC CORPORATE BRAND IDENTITY Business Card + Letterhead + Compliment Design\r\n\r\n2 Days Delivery\r\nUnlimited Revisions\r\nIncludes logo design\r\nLogo usage guidelines\r\nColor palette\r\nTypography guidelines',3,2,1),(7,'I will create an effective instagram hashtag growth strategy',522,20,'https://fiverrnew.cybersoft.edu.vn/images/cv5.jpg','\r\nHi There,\r\n\r\n\r\n\r\nHave You Been Looking for a Brand Logo with Complete Corporate Brand Identity?\r\n\r\n\r\n\r\nHi, I am Talha, a Passionate Professional Graphic Designer designing brand logos & brand Identity for many Years, I have successfully designed brand logos with the complete corporate brand identity for different brands around the world.\r\n\r\n\r\n\r\nI also would love to do demanded designs, Just leave your request, I will respond instantly.','#40 + organic growth strategy\r\nUS$20\r\n? 40# + Personalized Growth Strategy + Hashtag Guide + News List & Tools + Account Optimization\r\n\r\n3 Days Delivery\r\nPage/channel evaluation\r\nAction plan',5,4,3),(8,'I will design professional social media post',55,30,'https://fiverrnew.cybersoft.edu.vn/images/cv6.jpg','\r\nWelcome to our Social Media Posts Design.\r\n\r\nWe understand the client\'s needs and assist through the whole process with professionalism.\r\n\r\nWe are experienced in creating social media graphic content for clients all over the world, for business, products, and personal brands.\r\n\r\nWe can assist you to improve the appearance of your social media presence.','US$30\r\nSave up to 10% with Subscribe to Save\r\nBASIC 3 unique social media posts with the most attractive content that will transform your business.\r\n\r\n2 Days Delivery\r\n3 social posts created',4,2,3),(9,'I will setup facebook and instagram ads campaign to boost your business',53,25,'https://fiverrnew.cybersoft.edu.vn/images/cv7.jpg','Do you need a marketer with EXTREMELY WIDE experience in facebook and instagram ads? Well, you have come to the right place!\r\n\r\n\r\n\r\nI would love to boost your sales and revenue and Ensure Your Business Growth with Expertise in facebook and instagram ads. I will help you to get targeted customers and generate more QUALIFIED TRAFFIC, Leads, Message To make more sales.','Starting\r\nUS$25\r\n1 Campaign setup, multiple adsets and ads + Setup Marketing Goals+ Audience Research\r\n\r\n3 Days Delivery\r\nTarget audience research\r\nKeyword research\r\nAutomated feed ads (DPA)\r\nAd content creation\r\nAds analytical report\r\n3 days',2,5,3),(10,'I will do linkedin marketing and manage ads campaign',48,10,'https://fiverrnew.cybersoft.edu.vn/images/cv8.jpg','I am a professional social media marketing specialist.\r\n\r\nI have been working on LinkedIn Marketing, LinkedIn ads and provide quality services to my clients.\r\n\r\n\r\n\r\nLinkedin marketing is the way that provides you with an effective way to increase your business locally and globally.','LinkedIn page\r\nUS$10\r\nLinkedIn business page create and setup\r\n\r\n3 Days Delivery\r\nTarget audience research\r\nAutomated feed ads (DPA)\r\nAds analytical report\r\n3 days',5,5,3),(11,'I will write simple and interesting content for your website',288,30,'https://fiverrnew.cybersoft.edu.vn/images/cv9.jpg','My name is Kristina; I am not only a freelance writer but a professional basketball player too. I am currently jumping around Europe taking on some fun contracts and absorbing a bit of life along the way. I have had a passion for writing since I was a child and have been diving deeper into that passion over the last five years. I really enjoy writing about the immense number of random subjects asked of me (anything from travel, food, women empowerment and health to insects, art, professions and current events) and bring a flair for simple reading and easily digestible knowledge.','500 word article\r\nUS$30\r\nThis order includes a 500 word article based on your chosen topic.\r\n\r\n2 Days Delivery\r\n1 Revision\r\nUp to 500 words',5,7,3),(12,'I will write bulk SEO articles and blog post',91,5,'https://fiverrnew.cybersoft.edu.vn/images/cv10.jpg','MANDATORY REQUIREMENT: To ensure QUALITY is maintained, please send me a message BEFORE placing your order.\r\n\r\n\r\n\r\nI am providing my services at discounted rates to grow my account. You will never get top-rated service anywhere else!','1000 Words\r\nUS$5\r\n2x500 words or 1x1000 Words articles\r\n\r\n1 Day Delivery\r\nUp to 1,000 words\r\nSEO keywords',5,6,3),(13,'I will promote your brand on my twitter of over 155000 USA audience',22,5,'https://fiverrnew.cybersoft.edu.vn/images/cv11.jpg','I will create the Thumbnail, Place Title, Description & Tags also!\r\n\r\n\r\n\r\nI have 3,500 Subs!\r\n\r\nCurrent Fiverr Gig\r\n\r\n$60\r\n\r\nwww.youtube.com/channel/UCtuctzKeLfRgARcwpY0jJVw\r\n\r\n\r\n\r\nEXTRAS\r\n\r\nFaceCam-Camera $15\r\nRevision/Unlimited Revision $15\r\n1 Day Delivery $15\r\n\r\n\"Once Promo Is Approved & Completed! Videos Will Be Uploaded Shortly After!\r\n\r\nPlease give time for Videos to be Uploaded! Video Links Will Be Sent Same Day!\r\n\r\nTo Comply with YT Terms, Videos will have Disclaimer & Paid Promotions Info','Text/ link social media post\r\nUS$5\r\nExposure to a large audience with your custom Text+Link posted to my social media platform. Twitter\r\n\r\n3 Days Delivery',1,8,3),(14,'I will promote your nft or crypto project on my youtube channel',148,60,'https://fiverrnew.cybersoft.edu.vn/images/cv12.jpg','Hello, Welcome! I\'m Jordan!\r\n\r\n\r\n\r\nIf you need a Detailed Promotional Video, Then you are in the right place!\r\n\r\nI will cover any NFT Project or Crypto Project!\r\n\r\nWebsites, News, Articles, Social Medias, Connecting Wallets, ICOs, IDOs, IEOs, Tokenomics, Team, Roadmap, White Paper, Documents, Doxxed, KYCs, Audit Reports & Much More!\r\n\r\nI will explain how things work & any other important information needed!\r\n\r\nI will give my opinion on things as I talk about them!\r\n\r\nI will encourage people to join sales & socials to help grow your community!\r\n\r\nVideos are a minimum 8 minutes long!\r\n\r\nI will handle Editing, Graphic Effects & Music!\r\n\r\nDelivery is 2 days!','NFT & Crypto Youtube Promotion\r\nUS$60\r\nI will create and promote your NFT & Crypto Video on my Youtube Channel',3,8,3),(15,'I will do linkedin marketing and manage ads campaign',48,10,'https://fiverrnew.cybersoft.edu.vn/images/cv13.jpg','I am a professional social media marketing specialist.\r\n\r\nI have been working on LinkedIn Marketing, LinkedIn ads and provide quality services to my clients.\r\n\r\n\r\n\r\nLinkedin marketing is the way that provides you with an effective way to increase your business locally and globally.','LinkedIn page\r\nUS$10\r\nLinkedIn business page create and setup\r\n\r\n3 Days Delivery\r\nTarget audience research\r\nAutomated feed ads (DPA)\r\nAds analytical report\r\n3 days',4,10,3),(16,'I will setup facebook and instagram ads campaign to boost your business',53,25,'https://fiverrnew.cybersoft.edu.vn/images/cv14.jpg','Do you need a marketer with EXTREMELY WIDE experience in facebook and instagram ads? Well, you have come to the right place!\r\n\r\n\r\n\r\nI would love to boost your sales and revenue and Ensure Your Business Growth with Expertise in facebook and instagram ads. I will help you to get targeted customers and generate more QUALIFIED TRAFFIC, Leads, Message To make more sales.\r\n\r\nI will provide you a target audience with interests, behaviors, demographics and keywords','Starting\r\nUS$25\r\n1 Campaign setup, multiple adsets and ads + Setup Marketing Goals+ Audience Research\r\n\r\n3 Days Delivery\r\nTarget audience research\r\nKeyword research\r\nAutomated feed ads (DPA)\r\nAd content creation\r\nAds analytical report\r\n3 days',4,10,3);
/*!40000 ALTER TABLE `CongViec` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `LoaiCongViec`
--

DROP TABLE IF EXISTS `LoaiCongViec`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `LoaiCongViec` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ten_loai_cong_viec` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `LoaiCongViec`
--

LOCK TABLES `LoaiCongViec` WRITE;
/*!40000 ALTER TABLE `LoaiCongViec` DISABLE KEYS */;
INSERT INTO `LoaiCongViec` VALUES (1,'Graphics & Design'),(2,'Digital Marketing'),(3,'Writing & Translation'),(4,'Video & Animation'),(5,'Music & Audio'),(6,'Life Style'),(7,'Project Management'),(8,'Business Analyst'),(10,'string');
/*!40000 ALTER TABLE `LoaiCongViec` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `NguoiDung`
--

DROP TABLE IF EXISTS `NguoiDung`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `NguoiDung` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pass_word` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birth_day` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isAdmin` tinyint(1) NOT NULL,
  `skill` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `certification` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `NguoiDung`
--

LOCK TABLES `NguoiDung` WRITE;
/*!40000 ALTER TABLE `NguoiDung` DISABLE KEYS */;
INSERT INTO `NguoiDung` VALUES (1,'Vi','vi@gmail.com','$2b$10$4oqcqZzdMvQ1ZBfEuCFS8utlEs2SuBLByPnAQHaV2ikVeDMblp6va',NULL,'07777','1995','Male',1,NULL,NULL),(2,'string','string','$2b$10$pHP.HsKnX02NmW32q8oj7e7IDrK6yR99iTs2GZ.vgcxAJvtK4xa9a',NULL,'string','string','string',1,'string','string'),(3,'string','user@gmail.com','$2b$10$MKM9bnNFdZseXgSspI4Q6eLAWJ3JyaCw09yUGoHOq7hDlPVqyAtWW',NULL,'string','string','string',0,'string','string');
/*!40000 ALTER TABLE `NguoiDung` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ThueCongViec`
--

DROP TABLE IF EXISTS `ThueCongViec`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ThueCongViec` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ma_cong_viec` int NOT NULL,
  `ma_nguoi_thue` int NOT NULL,
  `ngay_thue` datetime(3) NOT NULL,
  `hoan_thanh` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ThueCongViec_ma_nguoi_thue_fkey` (`ma_nguoi_thue`),
  KEY `ThueCongViec_ma_cong_viec_fkey` (`ma_cong_viec`),
  CONSTRAINT `ThueCongViec_ma_cong_viec_fkey` FOREIGN KEY (`ma_cong_viec`) REFERENCES `CongViec` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `ThueCongViec_ma_nguoi_thue_fkey` FOREIGN KEY (`ma_nguoi_thue`) REFERENCES `NguoiDung` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ThueCongViec`
--

LOCK TABLES `ThueCongViec` WRITE;
/*!40000 ALTER TABLE `ThueCongViec` DISABLE KEYS */;
INSERT INTO `ThueCongViec` VALUES (2,2,2,'2023-03-11 13:19:12.214',1),(3,3,2,'2023-03-11 13:19:12.214',0),(5,5,2,'2023-03-14 11:42:50.984',1),(6,5,3,'2023-03-14 11:42:50.984',1),(7,7,3,'2023-03-14 11:44:32.164',0);
/*!40000 ALTER TABLE `ThueCongViec` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-04-05 22:15:49
